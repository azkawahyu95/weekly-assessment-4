const areaOfTriangle = (base,height) => {
    return ((base*height)/2)
}

const triArea1 = areaOfTriangle(3,2)
console.log(triArea1)
const triArea2 = areaOfTriangle(7,4)
console.log(triArea2)
const triArea3 = areaOfTriangle(10,10)
console.log(triArea3)