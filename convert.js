const hourToSecond = (hours) => {
    return hours * 3600
}

const howManySeconds1 = hourToSecond(2)
console.log(howManySeconds1)
const howManySeconds2 = hourToSecond(10)
console.log(howManySeconds2)
const howManySeconds3 = hourToSecond(24)
console.log(howManySeconds3)